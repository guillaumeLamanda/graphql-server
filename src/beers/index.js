export { default as beerData } from './data';
export { default as BeerAPI } from './datasource';
export { default as beerResolvers } from './resolvers';
export { default as beerTypeDef } from './typeDef';

export default {};
