export default {
  Query: {
    beers: (_, { page, pageSize }, { dataSources: { beerAPI } }) => beerAPI.getBeers({ page, pageSize }),
    beer: (_, { id }, { dataSources: { beerAPI } }) => beerAPI.getById({ beerId: id }),
  },
};
