/* eslint-disable class-methods-use-this */
import { RESTDataSource } from 'apollo-datasource-rest';

/**
 * @class BeerAPI
 * Beer datasource for apollo
 * Implement pink beer api : https://punkapi.com/documentation/v2
 */
export class BeerAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = 'https://api.punkapi.com/v2/beers';
  }

  /**
   * format beer response
   * @param {Beer} beer
   */
  beerReducer(beer) {
    return {
      id: beer.id || 0,
      name: beer.name,
      degree: beer.abv,
    };
  }

  /**
   * Get all beers
   * @param {Int} pageSize
   * @param {Int} page
   * @return {Array<Beer>}
   */
  async getBeers({ pageSize, page }) {
    const response = await this.get('', { page, per_page: pageSize });
    return Array.isArray(response)
      ? response.map(beer => this.beerReducer(beer))
      : [];
  }

  /**
   * Get one beer by id
   * @param {Int} beerId
   */
  async getById({ beerId }) {
    const response = await this.get(`${beerId}`);
    return response.length === 1 ? this.beerReducer(response[0]) : null;
  }

  /**
   * Get many beers by ids
   * @param {Array<Int>} beersIds
   */
  async getByIds({ beersIds }) {
    const response = await this.get(`?ids=${beersIds.join('|')}`);
    return response.map(beer => this.beerReducer(beer));
  }
}

export default BeerAPI;
