import { gql } from 'apollo-server';

export default gql`
  type Beer {
    id: Int!
    name: String!
    degree: Float
  }

  extend type Query {
    beers(pageSize: Int!, page: Int!): [Beer!]!
    beer(id: Int): Beer
  }
`;
