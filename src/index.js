import { ApolloServer, gql } from 'apollo-server';
import jwt from 'jsonwebtoken';

import { beerResolvers, beerTypeDef, BeerAPI } from './beers';
import { userResolvers, userTypeDef, UserDS } from './users';

const typeDef = gql`
  type Query
  type Mutation
  type Subscription
`;

const linkTypeDef = gql`
  extend type User {
    beers: [Beer!]!
  }

  type UserLike {
    name: String!
    beer: Beer!
  }

  extend type Query {
    usersBeersLikes: [UserLike!]!
  }

  extend type Subscription {
    # Définition des souscriptions
    userBeerLiked: UserLike
  }
`;

/** Instanciation du serveur */
const server = new ApolloServer({
  typeDefs: [typeDef, beerTypeDef, userTypeDef, linkTypeDef],
  resolvers: [beerResolvers, userResolvers],
  dataSources: () => ({
    beerAPI: new BeerAPI(),
    userDS: new UserDS(),
  }),
  context: ({ req, connection }) => {
    if (connection) return connection.context;
    try {
      return {
        userId: jwt.decode(req.headers.authorization).userId,
      };
    } catch (error) {
      return {};
    }
  },
});

server.listen(3000, () => {
  console.log('Server listening at http://localhost:3000');
});
