import { PubSub } from 'apollo-server';

export const pubsub = new PubSub();
export const USER_BEER_LIKED = 'USER_BEER_LIKED';

export default {};
