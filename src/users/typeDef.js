import { gql } from 'apollo-server';

export default gql`
  type User {
    id: Int!
    name: String!
    token: String
  }

  extend type Query {
    users: [User!]!
    user(id: Int): User
  }

  extend type Mutation {
    addUser(name: String!): User!
    toogleUserBeer(beerId: Int!): User
  }
`;
