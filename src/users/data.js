import { beerData as beers } from '../beers';

export default [
  {
    id: 0,
    name: 'John Doe',
    beers: [...beers],
  },
];
