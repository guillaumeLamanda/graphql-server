import { ApolloError } from 'apollo-server';
import { USER_BEER_LIKED, pubsub } from '../pubsub';

export default {
  Query: {
    users: (_, __, { dataSources: { userDS } }) => userDS.getAll(),
    user: (_, { id }, { dataSources: { userDS }, userId }) => (id || userId
      ? userDS.getById({ userId: id || userId })
      : new ApolloError('No id provided')),
    usersBeersLikes: async (_, __, { dataSources: { userDS, beerAPI } }) => {
      const users = await userDS.getAll();
      return (await Promise.all(
        users.map(async u => ({
          ...u.get(),
          beers: await beerAPI.getByIds({ beersIds: u.beers }),
        })),
      )).reduce(
        (accu, user) => [
          ...accu,
          ...user.beers.reduce(
            (baccu, beer) => [...baccu, { ...user, beer }],
            [],
          ),
        ],
        [],
      );
    },
  },
  Mutation: {
    addUser: (_, { name }, { dataSources: { userDS } }) => userDS.create({ name }),
    toogleUserBeer: async (
      _,
      { beerId },
      { dataSources: { userDS, beerAPI }, userId },
    ) => {
      const user = await userDS.getById({ userId });
      if (!user.beers.some(b => b === beerId)) {
        pubsub.publish(USER_BEER_LIKED, {
          userBeerLiked: {
            name: user.name,
            beer: beerAPI.getById({ beerId }),
          },
        });
      }
      return userDS.toogleBeer({ userId, beerId });
    },
  },
  Subscription: {
    userBeerLiked: {
      subscribe: () => pubsub.asyncIterator([USER_BEER_LIKED]),
    },
  },
  /**
   * On peut spécifier à apollo comment résoudre des champs
   * particuliers.
   * Ici, on récupère les objets bières à partir des Ids stockés
   * en base.
   */
  User: {
    beers: (user, __, { dataSources: { beerAPI } }) => beerAPI.getByIds({ beersIds: user.beers }),
  },
};
