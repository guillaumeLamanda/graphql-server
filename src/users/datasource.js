import { DataSource } from 'apollo-datasource';
import Sequelize from 'sequelize';
import jwt from 'jsonwebtoken';

/**
 * On créer le store du datasource, qui est une instance
 * sequelize ici.
 */
const createStore = () => {
  const sequelize = new Sequelize('atelier', 'brestjs', 'brestjs', {
    dialect: 'postgres',
    host: 'localhost',
    port: 5432,
  });

  /** On défini notre modele utilisateur */
  const User = sequelize.define('User', {
    name: Sequelize.STRING,
    beers: {
      type: Sequelize.ARRAY(Sequelize.INTEGER),
      defaultValue: [],
    },
  });

  sequelize.db = {
    User,
  };

  return sequelize;
};

/**
 * @class UserDS
 */
class UserDS extends DataSource {
  constructor() {
    super();
    this.store = createStore();
  }

  /**
   * Dans l'initialisation, on synchronise la base de données.
   * Si les tables n'existent pas, elles seront crées
   */
  async initialize() {
    await this.store.sync();
  }

  /**
   * Récupération de tous les utilisateurs en base.
   * @return {Array<User>}
   */
  async getAll() {
    return this.store.db.User.findAll();
  }

  /**
   * Récupération d'un utilisateur par sont Id
   * @param { Int } userId
   */
  async getById({ userId }) {
    return this.store.db.User.findByPk(userId);
  }

  /**
   * Création d'un utilisateur
   * @param {String}  name
   */
  async create({ name }) {
    const user = await this.store.db.User.create({ name });
    user.token = jwt.sign({ userId: user.id }, 'anAwesomePK');
    return user;
  }

  /**
   * Si l'id d'une bière est présente dans les favoris de l'utilisateur,
   * on la supprime, sinon on l'ajoute
   * @param {Int} userId
   * @param {Int} beerId
   */
  async toogleBeer({ userId, beerId }) {
    const user = await this.getById({ userId });
    if (user.beers.some(b => b === beerId)) return user.update({ beers: user.beers.filter(b => b !== beerId) });
    return user.update({ beers: [...user.beers, beerId] });
  }
}

export default UserDS;
