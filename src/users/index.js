export { default as userData } from './data';
export { default as UserDS } from './datasource';
export { default as userResolvers } from './resolvers';
export { default as userTypeDef } from './typeDef';

export default {};
