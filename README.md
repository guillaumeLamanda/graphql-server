# Les souscriptions

Nous y voilà ! La dernière étape de cet atelier, qui consiste à mettre en place les souscriptions !

L'idée ici est de notifier les clients lorsqu'une bière est aimé par un utilisateur, et vous allez voir, encore une fois, c'est plutôt simple.

## Définir un type UserLike

Pour que ce soit plus facile à consommer pour le client, on créer un nouveau type `UserLike`. Personnellement, je l'ai fais dans la variable `linkTypeDef` de `src/index`.

On défini un nouveau type qui associe un nom d'utilisateur à à une bière. Ma définition est donc la suivante :

```graphql
type UserLike {
  name: String!
  beer: Beer!
}
```

En réalité, pour bien le réaliser, il aurait été plus pertinant d'associer directement un utilisateur et un bière. J'ai cependant déjà écris le client, et le temps me manque pour réaliser la refracto.

Voilà à quoi ce type aurait du ressembler dans l'idéal :

```graphql
type UserLike {
  user: User!
  beer: Beer!
}
```

## Création du pubsub

Apollo nous met à disposition une classe qui va nous permettre de publier des événements : PubSub.

Afin de garder une certaine organisation, nous allons créer un nouveau dossier `src/pubsub`.  
Dans l'index, on va créer une instance PubSub et l'exporter, ainsi qu'une constante qui va permettre d'identifier la donnée publiée.

```js
import { PubSub } from "apollo-server";

export const pubsub = new PubSub();
export const USER_BEER_LIKED = "USER_BEER_LIKED";

export default {};
```

## Modification du contexte

Les souscriptions passent pas un transport websocket, tandis que les requêtes et mutations passent par les requêtes http. Cependant, avec Apollo, nous gérons ça de manière uniforme (à tord ou à raison, on pourra en discuter par la suite).  
Il va donc falloir faire une petite modification de notre contexte, dû à la différence des protocoles.

```js
context: ({ req, connection }) => {
    if (connection) return connection.context;
    try {
      return {
        userId: jwt.decode(req.headers.authorization).userId,
      };
    } catch (error) {
      return {};
    }
  },
```

## publication de l'événement

Ca y est, tout est prêt à présent pour publier notre événement.

Quant à la stratégie de l'implémentation, j'aurais personnellement préféré pouvoir le faire dans le datasource, au plus prêt de la donnée utilisateur, puis transformer l'id de la bière en objet bière, comme nous l'avons fait précédemment. Cependant, les websockets ne gèrent pas bien le contexte **et donc les datasources**. J'ai essayé et c'est impossible à l'heure actuelle. Il y a cependant une [issue ouverte](https://github.com/apollographql/apollo-server/issues/1526#issuecomment-412842378) là dessus.

On va donc l'implémenter dans le resolver.

La première chose à faire est de déclarer un objet `Subscription` au même niveau que Query et Mutation :

```js
Subscription: {
    userBeerLiked: {
      subscribe: () => pubsub.asyncIterator([USER_BEER_LIKED]),
    },
  },
```

On défini ici la clé de l'objet qui sera renvoyé au client.

Ensuite, on va pouvoir balancer l'évenement.  
Dans le resolver `toogleBeerLike`, on va devoir récupérer l'utilisateur, pour vérifier si la bière est présente dans sont tableau `beers`. Si elle ne l'ai pas, on publie l'événement. (Oui c'est une duplication de traitement, car cette vérification est déjà faite dans le datasource... Voilà pourquoi le voulais y implémenter l'événement.).

Si la bière n'est pas déjà en favoris, on publie l'événement qui concorde au type défini précédemment.

> Attention, le payload renvoyé doit être un objet contenant la clé de l'événement
>
> ```js
> {
>   userBeerLiked: {
>     // les datas
>   }
> }
> ```

<details>
<summary>Ma solution</summary>

```js
toogleUserBeer: async (
  _,
  { beerId },
  { dataSources: { userDS, beerAPI }, userId },
) => {
  const user = await userDS.getById({ userId });
  if (!user.beers.some(b => b === beerId)) {
    pubsub.publish(USER_BEER_LIKED, {
      userBeerLiked: {
        name: user.name,
        beer: beerAPI.getById({ beerId }),
      },
    });
  }
  return userDS.toogleBeer({ userId, beerId });
},
```

</details>

Et voilà ! Vous pouvez maintenant aller jouer dans le playground. Créez une nouvelle souscription (laissez vous guider par la complétion), puis ajoutez des favoris. Allez ensuite checker votre onglet souscriptions, et là, vous devriez les voir.

## Dernier mot sur GraphQL

Lors de cet atelier, j'ai tenté de vous faire découvrir nombre de principes de graphQL que je trouve important, cependant je n'ai pas le temps de vous faire découvrir la totalité des fonctionnalités de graphQL que je connais (et il y en a que je ne connais pas encore).

Par exemple, il est possible de finement gérer le cache des datasources, ce qui pourrait améliorer les performances de notre serveur. Il est aussi possible de "Mocker" les données afin que les devs fronts puissent commencer le développement avec des fake datas pendant que les back-end développent le serveur.

Allez faire un tour sur la doc et les exemples d'apollo pour aller plus loin 😉

## Petit mot de fin

Tout d'abord, si vous êtes arrivé jusqu'ici, notifiez le moi, ça me fera plaisir 😉

Merci d'avoir suivi cet atelier, j'espère qu'il vous a plu ! 🤙  
N'hésitez pas à me faire des retours sur ce que vous avez aimé ou pas. ✅❌

## Contribuez

Si vous avez des idées d'améliorations sur ce projet, n'hésitez pas à me soumettre des Merge Requests, c'est avec plaisir que je les accepteraient. 😉😉
